extends Node2D

@export var max_ite : float = 1000000

@onready var rand = RandomNumberGenerator.new()


var x : float = 0.0
var y : float = 0.0
var n : int = 0
var xn : float = 0.0
var yn : float = 0.0
var count : float = 0.0
var points = []
# Called when the node enters the scene tree for the first time.
func _ready():
	rand.randomize()
	loop()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _draw():
	for point in points:
		draw_circle(point * 500, 1, Color(0,1,0).darkened(count/max_ite))
	

func loop():
	while n < max_ite:
		var r = rand.randf_range(0.0, 1.0)
#		if n % 10000 == 0:
#			clean_points()
		if r < 0.1:
			xn = 0.0
			yn = 0.16 * y
			count += 5
		elif r < 0.86:
			xn = 0.85 * x + 0.04 * y
			yn = -0.04 * x + 0.85 * y + 1.6
		elif r < 0.93:
			xn = 0.2 * x - 0.26 * y
			yn = 0.23 * x + 0.22 * y + 1.6
		else:
			xn = -0.15 * x + 0.22 * y
			yn = 0.26 * x + 0.24 * y + 0.44
		if check_points_exist(Vector2(x, -y)):	
			points.append(Vector2(x, -y))
		queue_redraw()
		x = xn
		y = yn
		n += 1

func check_points_exist(coor):
	# check if the new point doeasn't already exist
	pass

func _on_run_really_pressed():
	n = 0
	count = 0
	loop()

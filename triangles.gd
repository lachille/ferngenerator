extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _draw():
	var A = $A.global_position + Vector2(5, 0)
	var B = $B.global_position + Vector2(5, 0)
	var C = $C.global_position + Vector2(5, 0)
	var D = $D.global_position + Vector2(5, 0)
	var E = $E.global_position + Vector2(5, 0)
	var F = $F.global_position + Vector2(5, 0)
	
	var color_line = Color(1,0,1, .3)
	draw_line(A, B, color_line, 1, true)
	draw_line(B, C, color_line, 1, true)
	draw_line(A, C, color_line, 1, true)
	
	var color_line2 = Color(1,1,0, .3)
	draw_line(D, E, color_line2, 1, true)
	draw_line(E, F, color_line2, 1, true)
	draw_line(D, F, color_line2, 1, true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


